import { createStackNavigator } from '@react-navigation/stack';
import { default as React } from 'react';
import { NAVIGATION } from 'src/constants';
import { QuanLyKenh } from 'src/screens';

const Stack = createStackNavigator();

//Menu footer App
export default KenhNavigator = () => {
  return (
    <Stack.Navigator initialRouteName={NAVIGATION.SCR_KENH}>
      <Stack.Screen name={NAVIGATION.SCR_KENH} component={QuanLyKenh} />
    </Stack.Navigator>
  );
};
