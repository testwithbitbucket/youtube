import { apiCaller, getKeyApiYoutube } from 'src/helper/AxiosClient';
const getItemVideoByVideo = async ({ id, cancelToken }) => {
  const KEY_API_YOUTUBE = await getKeyApiYoutube();

  const config = {
    method: 'GET',
    url: `videos`,
    cancelToken,
    params: {
      id,
      part: 'snippet,contentDetails,statistics,status',
      key: KEY_API_YOUTUBE,
    },
  };
  const res = await apiCaller(config);
  return res;
};

export default {
  getItemVideoByVideo,
};
