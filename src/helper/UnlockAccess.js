import { useSelector } from 'react-redux';
import { checkRoles } from './common';
/**
 * Kiểm tra component có quyền để hiển thị không
 * @export
 * @param {Array, Component} { requestRoles, children }
 * @return {*} 
 */
export default function UnlockAccess({ requestRoles, children }) {
  const auth = useSelector(state => state.auth);
  const isAllow = checkRoles(auth.roles, requestRoles);
  if (isAllow) {
    return children;
  }
  return null;
}
