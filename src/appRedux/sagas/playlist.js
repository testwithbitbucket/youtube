import { put, select, takeLatest } from "@redux-saga/core/effects";
import playlistApi from 'src/api/playlist';
import { getPlayListError, getPlayListResponse, setLoadingPlaylist } from "../actions/playlist";
import { PLAY_LIST_GET_REQUEST } from "../actionTypes";
import { playlistSelector } from "../selectors";

// Lấy danh sách trạng thái
function* getPlaylist() {
  try {
    const { meta } = yield select(playlistSelector);
    yield put(setLoadingPlaylist(true));
    const res = yield playlistApi.getPlayList(meta);
    if (res.status === 200) {
      yield put(getPlayListResponse(res.data));
    } else {
      yield put(getPlayListError('Có lỗi xảy ra'));
    }
  } catch (error) {
    yield put(getPlayListError(error));
  } finally {
    yield put(setLoadingPlaylist(false));
  }
}
export const watchGetPlayList = function* watchGetPlayList() {
  yield takeLatest(PLAY_LIST_GET_REQUEST, getPlaylist);
};
