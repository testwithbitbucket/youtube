import React, { useEffect, useState } from 'react';
import { Dimensions } from 'react-native';

const window = Dimensions.get("window");
const screen = Dimensions.get("screen");
const initPortrait = window.width <= window.height;
export default function useDimension() {
  const [isPortrait, setIsPortrait] = useState(initPortrait);
  const [dimensions, setDimensions] = useState({ window, screen })
  const { width, height } = dimensions.window;

  const onChange = ({ window, screen }) => {
    setDimensions({ window, screen });
  };

  useEffect(() => {
    Dimensions.addEventListener("change", onChange);
    return () => {
      Dimensions.removeEventListener("change", onChange);
    };
  });

  useEffect(() => {
    if (width >= height) {
      setIsPortrait(false)
    } else {
      setIsPortrait(true);
    }
  }, [width])

  return { isPortrait };
}
