import axios from 'axios';
import { getData, storeData } from './common';

export const VALUE_API_YOUTUBE = 'AIzaSyBLLAWVc8H9ZBbbBO35Pv0iSLNmN1tn6Rk';
export const VALUE_CHANNEL_YOUTUBE = 'UC-2gAq4LO7JOgZ7QBmrq7aQ';
export const KEY_API_YOUTUBE = 'key_api_youtube';
export const KEY_CHANNEL_YOUTUBE = 'key_channel_youtube';

const BASE_URL = 'https://www.googleapis.com/youtube/v3/';
export const getKeyApiYoutube = async () => {
  const key = await getData(KEY_API_YOUTUBE);
  if(!key) {
    await storeData(KEY_API_YOUTUBE, VALUE_API_YOUTUBE);
    return VALUE_API_YOUTUBE;
  }else{
    return key;
  }
};
export const getChannelYoutube = async () => {
  const key = await getData(KEY_CHANNEL_YOUTUBE);
  if(!key) {
    await storeData(KEY_CHANNEL_YOUTUBE, VALUE_CHANNEL_YOUTUBE)
    return VALUE_CHANNEL_YOUTUBE;
  }else{
    return key;
  }
};
const AxiosClient = axios.create({
  baseURL: BASE_URL,
  headers: {
    'Content-Type': 'application/json;charset=utf-8',
  },
});

AxiosClient.interceptors.request.use(
  async (config) => {
    config.params = {
      // add your default ones
      // key: KEY_API_YOUTUBE,
      // spread the request's params
      ...config.params,
    };
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

AxiosClient.interceptors.response.use(
  (response) => {
    if (response.status === 200 || response.status === 201) {
      return response;
    } else {
      throw new Error(response);
    }
  },
  async (error) => {
    if (error.response) {
      return Promise.reject(error.response);
    } else {
      return Promise.reject(error);
    }
  }
);

export const apiCaller = (config) => {
  return AxiosClient(config);
};
