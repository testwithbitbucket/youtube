import React from 'react';
import { StyleSheet, Text } from 'react-native';
import themeStyle from 'src/styles/theme.style';

export const Offline = () => {

  return (
    <Text style={styles.text}>
      Bạn đang ngoại tuyến, hãy kiểm tra kết nối mạng
    </Text>
  );
}

const styles = StyleSheet.create({
  text: {
    backgroundColor: themeStyle.BLUE_COLOR_LIGHT,
    textAlign: 'center',
    color: themeStyle.WHITE_COLOR,
    padding: 10,
    paddingHorizontal: 0
  }
});