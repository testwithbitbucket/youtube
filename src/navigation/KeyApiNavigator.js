import { createStackNavigator } from '@react-navigation/stack';
import { default as React } from 'react';
import { NAVIGATION } from 'src/constants';
import { QuanLyKeyApi } from 'src/screens';

const Stack = createStackNavigator();

//Menu footer App
export default KeyApiNavigator = () => {
  return (
    <Stack.Navigator initialRouteName={NAVIGATION.SCR_KEY_API}>
      <Stack.Screen name={NAVIGATION.SCR_KEY_API} component={QuanLyKeyApi} />
    </Stack.Navigator>
  );
};
