import { DON_VI_FAILED, DON_VI_REQUEST, DON_VI_RESPONSE, DU_AN_FAILED, DU_AN_REQUEST, DU_AN_RESPONSE, LIST_USER_FAILED, LIST_USER_REQUEST, LIST_USER_RESPONSE, TRANG_THAI_FAILED, TRANG_THAI_REQUEST, TRANG_THAI_RESPONSE } from "../actionTypes";

export function getTrangThai() {
  return {
    type: TRANG_THAI_REQUEST
  };
}

export function getTrangThaiFailed() {
  return {
    type: TRANG_THAI_FAILED,
  };
}

export function getTrangThaiResponse(response) {
  return {
    type: TRANG_THAI_RESPONSE,
    response
  };
}

export function getListUser() {
  return {
    type: LIST_USER_REQUEST
  };
}

export function getListUserFailed() {
  return {
    type: LIST_USER_FAILED,
  };
}

export function getListUserResponse(response) {
  return {
    type: LIST_USER_RESPONSE,
    response
  };
}

export function getDuAn() {
  return {
    type: DU_AN_REQUEST
  };
}

export function getDuAnFailed() {
  return {
    type: DU_AN_FAILED,
  };
}

export function getDuAnResponse(response) {
  return {
    type: DU_AN_RESPONSE,
    response
  };
}

export function getDonVi() {
  return {
    type: DON_VI_REQUEST
  };
}

export function getDonViFailed() {
  return {
    type: DON_VI_FAILED,
  };
}

export function getDonViResponse(response) {
  return {
    type: DON_VI_RESPONSE,
    response
  };
}