import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import themeStyle from 'src/styles/theme.style';
import DatePickerCustom from './DatePickerCustom';
import TextInputCustom from './TextInputCustom';
import TextareaItemCustom from './TextAreaItemCustom';
import UploadDoc from './UploadDoc';
import TextNormalCustom from './TextNormalCustom';

const InputCustom = ({ label, required = false, typeInput, ...rest }) => {

  /**
   * Hiển thị control theo typeInput
   * @return {*} 
   */
  const renderControl = () => {
    if (typeInput === 'text') {
      return <TextInputCustom {...rest} />
    }
    if (typeInput === 'textArea') {
      return <TextareaItemCustom {...rest} />
    }
    if (typeInput === "date") {
      return (
        <DatePickerCustom {...rest} />
      )
    }
    if (typeInput === "upload") {
      return (
        <UploadDoc {...rest} />
      )
    }
    return <TextNormalCustom {...rest} />
  }

  return (
    <View style={styles.container}>
      <View style={styles.wrapLabel}>
        <Text style={styles.inputLabel}>{label || ''}</Text>
        {required && <IconAntDesign name="exclamationcircleo" color="red" size={16} />}
      </View>
      {renderControl()}
    </View>
  )
}

export default InputCustom;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: themeStyle.WHITE_COLOR,
    paddingHorizontal: 14,
    marginVertical: 6
  },
  wrapLabel: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 2
  },
  inputLabel: {
    fontSize: 16,
    fontWeight: 'bold'
  }
})