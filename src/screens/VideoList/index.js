import { Tabs, Icon } from '@ant-design/react-native';
import { useIsFocused, useNavigation } from '@react-navigation/native';
import React, { useEffect, useLayoutEffect } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { getPlayList } from 'src/appRedux/actions/playlist';
import { NAVIGATION } from 'src/constants';
import commonStyle from 'src/styles/common.style';
import themeStyle from 'src/styles/theme.style';
import ListVideoAll from './TabAll/ListVideoAll';
import ListVideo from './TabSingle/ListVideo';

export const Home = () => {
  const { playlist } = useSelector((state) => state);
  const isFocused = useIsFocused();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const { meta, items } = playlist;
  const { pageToken } = meta;
  const newItems = [{ snippet: { title: 'Tất cả' }, id: 'all' }, ...items];

  useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Danh sách video',
      headerStyle: {
        backgroundColor: themeStyle.BLUE_COLOR_LIGHT,
      },
      headerTitleStyle: {
        fontSize: themeStyle.FONT_SIZE_TITLE_HEADER,
      },
      headerTintColor: themeStyle.WHITE_COLOR,
      headerLeft: () => {
        return (
          <TouchableOpacity onPress={() => navigation.navigate(NAVIGATION.APP)}>
            <Icon name="align-right" style={commonStyle.iconHeaderLeft} />
          </TouchableOpacity>
        );
      },
    });
  }, [navigation]);

  useEffect(() => {
    dispatch(getPlayList(pageToken));
  }, [pageToken]);

  useEffect(() => {
    dispatch(getPlayList(pageToken));
  }, [isFocused]);

  const renderListItem = (data) => {
    return data.map((item, index) => {
      if (item.id === 'all') {
        return <ListVideoAll data={items} key={index} />;
      } else {
        return <ListVideo id={item.id} key={index} />;
      }
    });
  };

  const renderTitleTabs = (items) => {
    const rs = items.map((item) => {
      return { title: item?.snippet?.title ?? 'Không tiêu đề' };
    });
    return rs;
  };

  return (
    <View style={styles.container}>
      {/* <SearchBar
        placeholder="Tìm kiếm"
        value={value}
        onChange={onChange}
        onCancel={clear}
        cancelText="Huỷ bỏ"
        style={styles.searchBar}
      /> */}
      <View style={{ flex: 1 }}>
        {items && items.length > 0 && (
          <Tabs tabs={renderTitleTabs(newItems)}>{renderListItem(newItems)}</Tabs>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  searchBar: {
    backgroundColor: '#f7f7f7',
  },
});
