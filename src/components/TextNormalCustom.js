import React from 'react'
import { StyleSheet, Text } from 'react-native'
import themeStyle from 'src/styles/theme.style'

export default function TextNormalCustom(props) {
  return <Text style={styles.inputItem}>{props.value}</Text>
}
const styles = StyleSheet.create({
  inputItem: {
    borderColor: themeStyle.GREY_COLOR_LIGHT,
    backgroundColor: '#dddddd',
    borderWidth: 1,
    fontSize: 14,
    paddingHorizontal: 8,
    height: 38,
    lineHeight: 38,

  }
})