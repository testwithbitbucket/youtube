import NetInfo from '@react-native-community/netinfo';
import { useEffect, useState } from 'react';

const useDetectNetwork = () => { // curry
  const [netInfo, setNetInfo] = useState(true);
  useEffect(() => {
    // Subscribe to network state updates
    const unsubscribe = NetInfo.addEventListener((state) => {
      setNetInfo(state.isConnected);
    });

    return () => {
      // Unsubscribe to network state updates
      unsubscribe();
    };
  }, []);
  return netInfo;
};
export default useDetectNetwork;