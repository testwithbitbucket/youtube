import { Flex, Modal } from '@ant-design/react-native';
import React from 'react';
import { SafeAreaView, Text, TouchableOpacity } from 'react-native';
import IconAntd from 'react-native-vector-icons/AntDesign';
import IconFeather from 'react-native-vector-icons/Feather';
import themeStyle from 'src/styles/theme.style';
function ModalPicker({ toggle, visible, children, title, reset, height }) {

  /**
   * Hiển thị header title
   * @return {*} 
   */
  const renderHeader = () => {
    return (
      <Flex justify="between" style={{ backgroundColor: themeStyle.GREY_COLOR_LIGHT, paddingHorizontal: 10 }}>
        {reset && <TouchableOpacity onPress={reset} style={{ paddingVertical: 10 }}><IconFeather size={24} color={themeStyle.COLOR_PLACEHOLDER} name="rotate-ccw" /></TouchableOpacity>}
        <Text style={{ paddingVertical: 10, fontSize: 16, color: themeStyle.COLOR_PLACEHOLDER }}>{title}</Text>
        <TouchableOpacity onPress={toggle} style={{ paddingVertical: 10 }}><IconAntd size={24} color={themeStyle.COLOR_PLACEHOLDER} name="close" /></TouchableOpacity>
      </Flex>
    )
  }

  return (
    <Modal
      popup
      onClose={toggle}
      animationType="slide-up"
      maskClosable
      visible={visible}
      closable
    >
      <SafeAreaView style={{ height: height ? height : 400 }}>
        {renderHeader()}
        {children}
      </SafeAreaView>
    </Modal>
  )
}

export default ModalPicker;
