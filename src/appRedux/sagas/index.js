//Saga effects
import { all, fork } from 'redux-saga/effects';
import { watchGetPlayList } from './playlist';

export default function* rootSaga() {
  yield all([
    fork(watchGetPlayList)
  ])
}