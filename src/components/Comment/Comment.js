import React from 'react'
import {
  Image, StyleSheet, Text, View, Alert
} from 'react-native';
import moment from 'moment';
import HTML from 'react-native-render-html';
import { useDispatch, useSelector } from 'react-redux';
import { commentDeleteRequest, commentSetItem } from 'src/appRedux/actions/comment';

export default function Comment({ comment }) {
  const { author_name, content, author_avatar_urls, date, id, author } = comment;
  const { idUser } = useSelector(state => state.auth);
  const noiDung = content.rendered;
  const dispatch = useDispatch();

  const setItemComment = () => {
    dispatch(commentSetItem(comment));
  }

  const handleDelete = () => {
    id && dispatch(commentDeleteRequest(id))
  }

  const onDelete = () => {
    Alert.alert(
      "Thông báo",
      "Bạn có chắc chắn xoá ?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => handleDelete() }
      ]
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.avatarContainer}>
        {author_avatar_urls["24"] && <Image
          resizeMode='contain'
          style={styles.avatar}
          source={{ uri: author_avatar_urls["24"] }}
        />}
      </View>
      <View style={styles.contentContainer}>
        <View style={styles.cardTop}>
          <Text style={styles.text} >{author_name}</Text>
          <Text style={[styles.text, styles.created]}>{moment(date).fromNow()}</Text>
        </View>
        <View style={[styles.text, styles.content]}>
          <HTML tagsStyles={{ p: { fontSize: 14, lineHeight: 24, paddingLeft: 0 }, a: { fontSize: 14 } }} source={{ html: noiDung }} />
        </View>
        {/* <Text
              style={[styles.text, styles.reply]}
              onPress={() => { navigation.navigate(NAVIGATION.SCR_CHI_TIET_COMMENT_BAO_CAO, { comment }) }}>
              Trả lời
            </Text> */}
        {author === idUser && (
          <Text style={styles.cardBottom} onPress={setItemComment}>Chỉnh sửa</Text>
        )}
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#EEE',
    paddingVertical: 10
  },
  avatarContainer: {
    alignItems: 'center',
    width: 50,
  },
  contentContainer: {
    flex: 1
  },
  avatar: {
    borderWidth: 1,
    borderColor: '#EEE',
    borderRadius: 13,
    width: 26,
    height: 26,
  },
  text: {
    color: '#000',
    fontFamily: 'Avenir',
    fontSize: 13
  },
  textBottom: {
    color: '#333',
    fontSize: 12
  },
  name: {
    fontWeight: 'bold',
  },
  cardTop: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  cardBottom: {
    marginTop: 10,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  cardBottom: {
    display: 'flex',
    textAlign: 'right',
  },
  textDelete: {
    marginLeft: 10
  },
  reply: {
    color: '#333'
  },
  created: {
    color: '#BBB',
  },
});
