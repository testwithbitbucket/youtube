import { Button, Icon } from '@ant-design/react-native';
import { default as React, useEffect, useLayoutEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import InputCustom from 'src/components/InputCustom';
import { getData, storeData } from 'src/helper/common';
import { Toast } from '@ant-design/react-native';
import { KEY_CHANNEL_YOUTUBE } from 'src/helper/AxiosClient';
import { useNavigation } from '@react-navigation/native';
import themeStyle from 'src/styles/theme.style';
import commonStyle from 'src/styles/common.style';

export const QuanLyKenh = () => {
  const navigation = useNavigation();
  const [key, setKey] = useState('');
  const {
    control,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Quản lý kênh',
      headerStyle: {
        backgroundColor: themeStyle.BLUE_COLOR_LIGHT,
      },
      headerTitleStyle: {
        fontSize: themeStyle.FONT_SIZE_TITLE_HEADER,
      },
      headerTintColor: themeStyle.WHITE_COLOR,
      headerLeft: () => {
        return (
          <TouchableOpacity onPress={() => navigation.openDrawer()}>
            <Icon name="align-right" style={commonStyle.iconHeaderLeft} />
          </TouchableOpacity>
        );
      },
    });
  }, [navigation]);

  useEffect(async () => {
    const getKeyChanel = async () => {
      const keyChannel = await getData(KEY_CHANNEL_YOUTUBE);
      setKey(keyChannel);
    };
    getKeyChanel();
  }, []);

  const saveKey = async (value) => {
    try {
      await storeData(KEY_CHANNEL_YOUTUBE, value);
    } catch (e) {
      Toast.info('Sai mật khẩu', 0.3);
      // saving error
    }
  };

  const clearKey = () => {
    setKey('');
    saveKey('');
  };

  /**
   * Hàm handle submit
   * @param {*} values
   */
  const onSubmit = (values) => {
    const { key, password } = values;
    if (password === '123') {
      setKey(key);
      saveKey(key);
    } else {
      Toast.info('Sai mật khẩu', 0.3);
    }
  };

  const handleClickKey = () => {
    setValue('key', key);
  };

  return (
    <View style={styles.container}>
      <ScrollView>
        <Text onPress={handleClickKey} style={styles.keyText}>
          {key ? key : 'No key'}
        </Text>
        <Controller
          control={control}
          render={({ field: { onChange, onBlur, value } }) => (
            <InputCustom
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
              value={value}
              label="Mã kênh"
              placeholder="Mã kênh"
              typeInput="text"
            />
          )}
          name="key"
          defaultValue=""
        />
        <Controller
          control={control}
          render={({ field: { onChange, onBlur, value } }) => (
            <InputCustom
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
              value={value}
              label="Mật khẩu"
              placeholder="Nhập mật khẩu"
              typeInput="text"
            />
          )}
          defaultValue=""
          name="password"
        />
        <View style={styles.groupBtn}>
          <Button style={styles.btn} onPress={handleSubmit(onSubmit)} type="primary">
            <Text>Cập nhật</Text>
          </Button>
        </View>
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  keyText: {
    textAlign: 'center',
    fontSize: 16,
    color: '#18A6F0',
    fontWeight: 'bold',
    padding: 12,
  },
  groupBtn: {
    padding: 16,
  },
  innerForm: {
    backgroundColor: '#fff',
  },
});
