import React, { useRef, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, Dimensions } from 'react-native';
import Icon from "react-native-vector-icons/AntDesign";
import { ScaledSheet } from 'react-native-size-matters';
import themeStyle from 'src/styles/theme.style';

const { height } = Dimensions.get('window');
const { width } = Dimensions.get('window');
export default function FabButton({ icon, onPress, arr = [] }) {
  const animation = useRef(new Animated.Value(0)).current;
  const [isOpen, setIsOpen] = useState(false);

  const toggleOpen = () => {
    const toValue = isOpen ? 0 : 1;
    Animated.timing(animation, {
      toValue,
      duration: 200,
      useNativeDriver: true
    }).start();
    setIsOpen(prevState => !prevState);
  }

  const labelPositionInterpolate = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [-30, -60],
  });

  const opacityInterpolate = animation.interpolate({
    inputRange: [0, 0.8, 1],
    outputRange: [0, 0, 1],
  });

  const labelStyle = {
    transform: [
      {
        translateX: labelPositionInterpolate,
      },
    ],
  };

  const scaleInterpolate = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [0, height / 60 * 2],
  });

  const bgStyle = {
    transform: [
      {
        scale: scaleInterpolate,
      },
    ],
  };

  const renderChildrenButton = (arr) => {
    return arr.map((item, index) => (
      <TouchableWithoutFeedback
        key={index}
        onPress={() => {
          item.onPress && item.onPress();
        }}
      >
        <Animated.View style={[styles.button, styles.other, {
          transform: [
            { scale: animation },
            {
              translateY: animation.interpolate({
                inputRange: [0, 1],
                outputRange: [0, -70 * (index + 1)],
              }),
            },
          ],
        }]}>
          {/* <Animated.Text style={[styles.label, labelStyle]}>{item.title}</Animated.Text> */}
          <Icon name={item.icon} size={20} color="#555" />
        </Animated.View>
      </TouchableWithoutFeedback >
    ))
  }

  return (
    <View style={styles.container}>
      <Animated.View style={[styles.background, bgStyle]} />
      {arr.length > 0
        ? (
          <>
            {renderChildrenButton(arr)}
            <TouchableWithoutFeedback onPress={toggleOpen} >
              <View style={[styles.button, styles.mainButton]}>
                <Icon name={icon} size={20} style={styles.iconMainButton} />
              </View>
            </TouchableWithoutFeedback>
          </>
        )
        : (
          <TouchableWithoutFeedback onPress={onPress} >
            <View style={[styles.button, styles.mainButton]}>
              <Icon name={icon} size={20} style={styles.iconMainButton} />
            </View>
          </TouchableWithoutFeedback>
        )
      }
    </View>
  )
}

const WIDTH = '60@ms0.3';
const HEIGHT = '60@ms0.3';
const BORDER_RADIUS = '30@ms0.3';

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  label: {
    color: "#FFF",
    position: "absolute",
    fontSize: '18@ms0.3',
    fontWeight: 'bold',
    backgroundColor: "transparent",
  },
  button: {
    width: WIDTH,
    height: HEIGHT,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "#333",
    shadowOpacity: 0.1,
    shadowOffset: { x: 2, y: 0 },
    shadowRadius: 2,
    borderRadius: BORDER_RADIUS,
    position: "absolute",
    bottom: 20,
    right: 20,
    alignSelf: 'flex-start'
  },
  mainButtonText: {
    color: "#FFF",
  },
  iconMainButton: {
    color: "#FFF",
  },
  mainButton: {
    backgroundColor: themeStyle.BLUE_COLOR_LIGHT,
  },
  background: {
    backgroundColor: "rgba(0,0,0,.2)",
    position: "absolute",
    width: WIDTH,
    height: HEIGHT,
    bottom: 20,
    right: 20,
    borderRadius: BORDER_RADIUS,
  },
  other: {
    backgroundColor: "#FFF",
  },
});
