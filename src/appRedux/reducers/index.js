import { combineReducers } from 'redux';
import { getPlaylistReducer } from './playlist';

const rootReducer = combineReducers({
  playlist: getPlaylistReducer,
});

export default rootReducer;
