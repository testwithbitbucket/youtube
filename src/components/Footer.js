import React from 'react';
import { Text } from 'react-native';
import commonStyle from 'src/styles/common.style';

export function FooterIsEmpty() {
  return (
    <Text style={commonStyle.footerList}>Dữ liệu rỗng</Text>
  )
}

export function FooterIsLast() {
  return (
    <Text style={commonStyle.footerList}>Bạn đã ở trang cuối cùng</Text>
  )
}