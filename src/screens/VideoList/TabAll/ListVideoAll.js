import React from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import ListVideoPlayList from './ListVideoPlayList';
import Icon from 'react-native-vector-icons/Foundation';

const ListVideoAll = ({ data }) => {

  return (
    <View style={styles.listVideo}>
      <ScrollView>
        {data.map((item, index) => {
          return (
            <View key={index} >
              <View style={styles.heading}>
                <Icon name="play-video" size={18} />
                <Text style={styles.headingTitle}>
                  {item?.snippet?.title ?? 'Chưa có tiêu đề'}
                </Text>
              </View>
              <ListVideoPlayList id={item.id} />
            </View>
          )
        })}
      </ScrollView>
    </View>
  )
}
export default ListVideoAll;
const styles = StyleSheet.create({
  heading: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 8,
    marginBottom: 4
  },
  headingTitle: {
    fontWeight: 'bold',
    marginLeft: 8
  },
  listVideo: {
    flex: 1,
    backgroundColor: '#E5E5E5'
  }
})