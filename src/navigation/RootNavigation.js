import * as React from 'react';
import { StackActions } from '@react-navigation/native';

export const navigationRef = React.createRef();
export const isReadyRef = React.createRef();

export function navigate(name, params) {
  if (isReadyRef.current) {
    if (params) {
      navigationRef.current.navigate(name, params);
    } else {
      navigationRef.current.navigate(name);
    }
  }
}
export function push(...args) {
  navigationRef.current?.dispatch(StackActions.push(...args));
}