import { PLAY_LIST_GET_SUCCESS, PLAY_LIST_GET_REQUEST, PLAY_LIST_GET_ERROR, PLAY_LIST_GET_SET_LOADING } from "../actionTypes";

export function setLoadingPlaylist(payload) {
  return {
    type: PLAY_LIST_GET_SET_LOADING,
    payload
  };
}
export function getPlayList(payload) {
  return {
    type: PLAY_LIST_GET_REQUEST,
    payload
  };
}

export function getPlayListResponse(payload) {
  return {
    type: PLAY_LIST_GET_SUCCESS,
    payload
  };
}

export function getPlayListError(error) {
  return {
    type: PLAY_LIST_GET_ERROR,
    error
  };
}

