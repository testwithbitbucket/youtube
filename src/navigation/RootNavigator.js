import { createStackNavigator } from '@react-navigation/stack';
import { default as React } from 'react';
import { NAVIGATION } from 'src/constants';
import AppNavigator from './AppNavigator';
import { NavigationContainer } from './NavigationContainer';
import VideoNavigator from './VideoNavigator';

const Stack = createStackNavigator();

export const Root = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        cardOverlayEnabled: false,
        gestureEnabled: false,
      }}
    >
      <Stack.Screen
        name={NAVIGATION.SCR_VIDEO_LIST}
        options={{ headerShown: false }}
        component={VideoNavigator}
      />
      <Stack.Screen
        name={NAVIGATION.APP}
        options={{ headerShown: false }}
        component={AppNavigator}
      />
    </Stack.Navigator>
  );
};
export const RootNavigator = () => (
  <NavigationContainer>
    <Root />
  </NavigationContainer>
);
