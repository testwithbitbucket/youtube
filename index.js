/**
 * @format
 */
// Đối với ios cần import thư viện react-native-gesture-handler 
import 'react-native-gesture-handler';

import { AppRegistry } from 'react-native';
import NextApp from './NextApp';
import { name as appName } from './app.json';

AppRegistry.registerComponent(appName, () => NextApp);
