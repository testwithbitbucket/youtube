import { createStackNavigator } from '@react-navigation/stack';
import { default as React } from 'react';
import { NAVIGATION } from 'src/constants';
import { VideoDetail, Home } from 'src/screens';

const Stack = createStackNavigator();

//Menu footer App
export default VideoNavigator = () => {
  return (
    <Stack.Navigator initialRouteName={NAVIGATION.SCR_VIDEO_LIST}>
      <Stack.Screen name={NAVIGATION.SCR_VIDEO_LIST} component={Home} />
      <Stack.Screen
        name={NAVIGATION.SCR_VIDEO_DETAIL}
        options={{ headerShown: false }}
        component={VideoDetail}
      />
    </Stack.Navigator>
  );
};
