import DocumentPicker from 'react-native-document-picker';

export const typeFile = [
  DocumentPicker.types.docx,
  DocumentPicker.types.doc,
  DocumentPicker.types.images,
]