import { Button, List } from '@ant-design/react-native';
import React, { useState } from 'react';
import { ScrollView, View } from 'react-native';
import useModal from 'src/hooks/useModal';
import themeStyle from 'src/styles/theme.style';
import RadioButton from './RadioButton';

const Item = List.Item;

const ListSelectChoose = (props) => {
  const { value, data, onChange, title, reset } = props;
  const { isShowing, toggle } = useModal();
  const [currentValue, setCurrentValue] = useState(value);

  const handleChangeList = (currentValue) => {
    setCurrentValue(currentValue);
  }

  const handleChange = () => {
    onChange(currentValue);
    toggle();
  }

  const getLabel = () => {
    const item = data.find(item => item.value === currentValue);
    return item && item.label || 'Mặc định';
  }

  const option = data.map((item, index) => {
    return (
      <Item
        onPress={() => handleChangeList(item.value)}
        key={index}
        extra={
          <RadioButton selected={item.value === currentValue} />
        }
      >
        {item.label}
      </Item>
    )
  })

  return (
    <View>
      <View style={{ backgroundColor: themeStyle.WHITE_COLOR }}>
        <Item onPress={toggle} extra={getLabel()} arrow="horizontal">
          {title}
        </Item>
      </View>
      <ModalPicker
        toggle={toggle}
        visible={isShowing}
        reset={reset}
      >
        <ScrollView>
          <List renderHeader={title}>
            {option}
          </List>
        </ScrollView>
        <Button type="primary" onPress={handleChange} style={{ margin: 14 }}>
          Chọn
        </Button>
      </ModalPicker>
    </View>
  )
}

export default ListSelectChoose;