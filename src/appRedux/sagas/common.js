import { put, takeLatest } from "@redux-saga/core/effects"
import { DON_VI_REQUEST, DU_AN_REQUEST, LIST_USER_REQUEST, TRANG_THAI_REQUEST } from "../actionTypes";
import commonApi from 'src/api/common';
import {
  getDonViFailed, getDonViResponse,
  getDuAnFailed, getDuAnResponse,
  getTrangThaiFailed, getTrangThaiResponse,
  getListUserFailed, getListUserResponse
} from "../actions/common";

// Lấy danh sách trạng thái
function* getTrangThai() {
  try {
    const { response } = yield commonApi.getListTrangThai();
    const dataRecieve = response.data.map((item) => {
      const { name, slug, id, link } = item;
      const newItem = { name, slug, id, link };
      return newItem;
    })
    yield put(getTrangThaiResponse(dataRecieve));
  } catch (error) {
    yield put(getTrangThaiFailed());
  }
}
export const watchFetchTrangThai = function* watchFetchTrangThai() {
  yield takeLatest(TRANG_THAI_REQUEST, getTrangThai);
};

// Lấy danh sách dự án
function* getDuAn() {
  try {
    const dataRecieve = yield commonApi.getListDuAn();
    const { response } = dataRecieve;
    yield put(getDuAnResponse(response));
  } catch (error) {
    yield put(getDuAnFailed());
  }
}
export const watchFetchDuAn = function* watchFetchDuAn() {
  yield takeLatest(DU_AN_REQUEST, getDuAn);
};

// Lấy danh sách đơn vị
function* getDonVi() {
  try {
    const dataRecieve = yield commonApi.getListDonVi();
    const { response } = dataRecieve;
    yield put(getDonViResponse(response));
  } catch (error) {
    yield put(getDonViFailed());
  }
}
export const watchFetchDonVi = function* watchFetchDonVi() {
  yield takeLatest(DON_VI_REQUEST, getDonVi);
};

// Lấy danh sách user
function* getListUser() {
  try {
    const dataRecieve = yield commonApi.getListUser();
    const { response } = dataRecieve;
    yield put(getListUserResponse(response));
  } catch (error) {
    yield put(getListUserFailed(error));
  }
}
export const watchFetchListUser = function* watchFetchListUser() {
  yield takeLatest(LIST_USER_REQUEST, getListUser);
};