import React from 'react';
import { ScrollView, Image, View, Text } from 'react-native';
import { Card } from '@ant-design/react-native';

export const About = () => {

  // Hiển thị footer
  const renderFooter = () => {
    return (
      <Text>QTI Mobile Tearm</Text>
    )
  }

  return (
    <View flex={1}>
      <ScrollView>
        <Card>
          <Card.Header
            title={<Text style={{ color: '#000' }}>THÔNG TIN VỀ ỨNG DỤNG</Text>}
            style={{ paddingVertical: 10 }}
          />
          <Card.Body>
            <View style={{ alignItems: 'center', width: '100%', paddingBottom: 10 }}>
              <Image source={require('src/assets/images/pagehome/mac.png')} style={{ width: 120, height: 120, }} />
            </View>
            <View style={{ marginHorizontal: 16 }}>
              <Text>Đây là ứng dụng giúp theo dõi công việc nhóm, cá nhân, và chia sẽ 1 vài kiến thức liên quan đến web, giao diện</Text>
              <Text>Mọi góp ý của các bạn vui lòng liên hệ:</Text>
              <Text>Mail: designerqn@gmail.com</Text>
            </View>
          </Card.Body>
          <Card.Footer
            content={renderFooter()}
          />
        </Card>
      </ScrollView>
    </View >
  );
};
