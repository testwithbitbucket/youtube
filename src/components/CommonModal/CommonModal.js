import React from 'react';
import Modal from 'react-native-modal';
export const CommonModalComponent = (props) => {
  const { children, modal } = props;
  const { isOpen } = modal;
  return (
    <Modal isVisible={isOpen}>
      {children}
    </Modal>
  )
}
