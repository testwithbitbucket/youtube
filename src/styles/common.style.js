import { StyleSheet, Platform, StatusBar } from 'react-native';
import themeStyle from './theme.style';
import theme from './theme.style';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

export default StyleSheet.create({
  container: {
    flex: 1
  },
  cardItem: {
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 15,
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 15,
    paddingBottom: 25,
    borderRadius: 15,
    borderLeftWidth: 0.5,
    borderRightWidth: 0.5,
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    borderColor: theme.BORDER_COLR,
    flexWrap: "nowrap",
    backgroundColor: theme.WHITE_COLOR,
    shadowColor: theme.BLACK_COLOR,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.1,
    shadowRadius: 1.5,
    elevation: 0
  },
  cardTitle: {
    color: theme.DARK_COLOR_LIGHT,
    fontWeight: 'bold',
    textAlign: 'justify',
    fontSize: 16
  },
  iconHeaderLeft: {
    color: theme.WHITE_COLOR,
    fontSize: theme.FONT_SIZE_ICON_HEADER,
    paddingLeft: 8
  },
  iconHeaderRight: {
    color: theme.WHITE_COLOR,
    fontSize: theme.FONT_SIZE_ICON_HEADER,
    paddingRight: 8
  },
  titleHeader: {
    color: theme.WHITE_COLOR,
    fontSize: theme.FONT_SIZE_TITLE_HEADER
  },
  footerList: {
    marginBottom: Platform.OS === 'ios' ? 20 : 0,
    textAlign: 'center',
    backgroundColor: themeStyle.WHITE_COLOR
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
    backgroundColor: theme.BLUE_COLOR_BOLD,
    zIndex: 0
  }
});