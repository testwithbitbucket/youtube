import { apiCaller, getKeyApiYoutube } from 'src/helper/AxiosClient';
const getPlayListItem = async ({ cancelToken, ...data }) => {
  const KEY_API_YOUTUBE = await getKeyApiYoutube();
  const config = {
    method: 'GET',
    url: `playlistItems`,
    cancelToken,
    params: {
      ...data,
      part: 'contentDetails,id,snippet,status',
      key: await KEY_API_YOUTUBE,
    },
  };
  const res = await apiCaller(config);
  return res;
};

export default {
  getPlayListItem,
};
