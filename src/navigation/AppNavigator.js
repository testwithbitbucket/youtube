import { createDrawerNavigator } from '@react-navigation/drawer';
import { default as React } from 'react';
import { NAVIGATION } from 'src/constants';
import { SideBar } from 'src/screens';
import KenhNavigator from './KenhNavigator';
import KeyApiNavigator from './KeyApiNavigator';

const Drawer = createDrawerNavigator();

//App navigator
const AppNavigator = () => {
  return (
    <Drawer.Navigator
      initialRouteName={NAVIGATION.DRAWER_KENH}
      drawerContent={(props) => <SideBar {...props} />}
    >
      <Drawer.Screen name={NAVIGATION.DRAWER_KENH} component={KenhNavigator} />
      <Drawer.Screen name={NAVIGATION.DRAWER_API} component={KeyApiNavigator} />
    </Drawer.Navigator>
  );
};

export default AppNavigator;
