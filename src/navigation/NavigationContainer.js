import { NavigationContainer as RNNavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import * as React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { navigationRef, isReadyRef } from './RootNavigation';

export const NavigationContainer = ({ colorScheme, children }) => {
  React.useEffect(() => {
    return () => {
      isReadyRef.current = false
    };
  }, []);
  return (
    <SafeAreaProvider>
      <RNNavigationContainer
        theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}
        ref={navigationRef}
        onReady={() => {
          isReadyRef.current = true;
        }}
      >
        {children}
      </RNNavigationContainer>
    </SafeAreaProvider>
  );
}
