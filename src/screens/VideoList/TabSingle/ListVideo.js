import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import playlistItemApi from 'src/api/playlistItem';
import { FooterIsEmpty } from 'src/components/Footer';
import ItemVideo from './ItemVideo';

const ListVideo = ({ id }) => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);

  useEffect(() => {
    let isCancelled = false;
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();
    const getListItemVideo = async (id) => {
      try {
        setLoading(true);
        const res = await playlistItemApi.getPlayListItem({ playlistId: id, maxResults: 10, cancelToken: source.token });
        if (res.status === 200 && !isCancelled) {
          setData(res.data.items);
        } else {
          throw new Error('Có lỗi xảy ra');
        }
      } catch (error) {
        if (axios.isCancel(error)) {
          console.log("cancelled");
        } else {
          throw error;
        }
      } finally {
        if (!isCancelled) {
          setLoading(false);
        }
      }
    };
    if (id) {
      getListItemVideo(id);
    }
    return () => {
      source.cancel();
      isCancelled = true;
    };
  }, [id])

  const renderItem = ({ item }) => {
    return <ItemVideo item={item} playlistId={id} />
  }

  return (
    <View style={styles.listVideo}>
      <FlatList
        data={data}
        extraData={data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={renderItem}
        initialNumToRender={10}
        // onEndReached={handleLoadMore}
        onEndReachedThreshold={0.5}
        // ListFooterComponent={renderFooter()}
        ListEmptyComponent={!loading && <FooterIsEmpty />}
      // refreshControl={<RefreshControl
      //   colors={["#9Bd35A", "#689F38"]}
      // // refreshing={refreshing}
      // // onRefresh={onRefresh}
      // />}
      />
      {/* {!isConnected && <Offline />} */}
    </View>
  )
}
export default ListVideo;
const styles = StyleSheet.create({
  listVideo: {
    flex: 1,
    backgroundColor: '#E5E5E5'
  }
})