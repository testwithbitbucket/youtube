import { decode } from 'html-entities';
import { Dimensions, PixelRatio } from 'react-native';
import HTML from 'react-native-render-html';
import { NAVIGATION } from 'src/constants';
import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

// Xử lý chuỗi html
export const htmlDecode = (html) => {
  return decode(html);
};

// Xử lý chuỗi html
export const trimTabHtml = (html) => {
  const regex = /(<([^>]+)>)/gi;
  const result = html.replace(regex, '');
  return result;
};

// Lấy chiều rộng theo phần trăm chiều rộng màn hình
export const widthPercentageToDP = (widthPercent) => {
  const screenWidth = Dimensions.get('window').width;
  // Convert string input to decimal number
  const elemWidth = parseFloat(widthPercent);
  return PixelRatio.roundToNearestPixel((screenWidth * elemWidth) / 100);
};

// Lấy chiều cao theo phần trăm chiều cao màn hình
export const heightPercentageToDP = (heightPercent) => {
  const screenHeight = Dimensions.get('window').height;
  // Convert string input to decimal number
  const elemHeight = parseFloat(heightPercent);
  return PixelRatio.roundToNearestPixel((screenHeight * elemHeight) / 100);
};

// Lấy màn hình theo loại notify
export const getScreenByNotify = (type) => {
  let typeScreen;
  switch (type) {
    case 'qlcv':
      typeScreen = {
        main: NAVIGATION.DRAWER_QLCV,
        screen: NAVIGATION.SCR_CHI_TIET_CV,
        type,
      };
      break;
    case 'report':
      typeScreen = {
        main: NAVIGATION.DRAWER_BAO_CAO_CV,
        screen: NAVIGATION.SCR_CHI_TIET_BAO_CAO_CV,
        type,
      };
      break;
    default:
      break;
  }
  return typeScreen;
};

// Kiểm tra roles
export const checkRoles = (roles, requestRoles) => {
  if (Array.isArray(roles) && Array.isArray(requestRoles) && requestRoles.length) {
    if (roles.includes('administrator')) {
      return true;
    }
    if (requestRoles.some((role) => roles.includes(role))) {
      return true;
    }
    return false;
  }
  return false;
};

// Render dữ liệu html
export const renderHtml = (html) => {
  if (html) {
    return (
      <HTML
        tagsStyles={{ p: { fontSize: 14, lineHeight: 24, paddingLeft: 10 }, a: { fontSize: 14 } }}
        source={{ html }}
      />
    );
  }
  return null;
};

export const storeData = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    // saving error
  }
};

export const getData = async (key) => {
  try {
    const value = await AsyncStorage.getItem(key);
    return value;
  } catch (e) {
    // error reading value
    return null;
  }
};
