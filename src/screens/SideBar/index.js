import { List } from '@ant-design/react-native';
import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import { Image, ScrollView, Text, View } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import { NAVIGATION } from 'src/constants';

const initRoutes = [
  {
    name: NAVIGATION.SCR_VIDEO_LIST,
    icon: require('src/assets/images/home.png'),
    title: 'Trang chủ',
  },
  {
    name: NAVIGATION.DRAWER_KENH,
    icon: require('src/assets/images/list.png'),
    title: 'Quản lý kênh',
  },
  {
    name: NAVIGATION.DRAWER_API,
    icon: require('src/assets/images/list.png'),
    title: 'Quản lý API',
  },
];

export function SideBar() {
  const navigation = useNavigation();
  const [routes, setRoutes] = useState(initRoutes);

  // Xử lý khi nhấn menu
  const handlePress = (item) => {
    navigation.navigate(item.name);
  };

  // Hiển thị danh sách menu
  const renderRoutes = () => {
    return routes.map((item, index) => {
      return (
        <List.Item key={index} onPress={() => handlePress(item)}>
          <View style={styles.containerMenu}>
            <Image source={item.icon} style={styles.iconImage} />
            <Text style={styles.titleMenu}>{item.title}</Text>
          </View>
        </List.Item>
      );
    });
  };

  return (
    <ScrollView style={styles.container}>
      <View style={styles.container}>
        <View style={styles.top}>
          <Image source={require('src/assets/images/drawer-cover.png')} style={styles.bgImage} />
          <Image
            style={styles.logoImage}
            square
            source={require('src/assets/images/logoMini.png')}
          />
        </View>
        <List style={styles.lstMenu}>{renderRoutes()}</List>
      </View>
    </ScrollView>
  );
}
const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  bgImage: {
    height: '80@ms0.3',
    width: '100%',
    position: 'absolute',
  },
  top: {
    height: '80@ms0.3',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
  },
  logoImage: {
    height: '60@ms0.3',
    width: '65@ms0.3',
  },
  userInfo: {
    marginLeft: '10@ms0.3',
  },
  containerMenu: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: '8@ms0.3',
  },
  iconImage: {
    width: '30@ms0.3',
    height: '30@ms0.3',
    marginRight: '10@ms0.3',
  },
  titleMenu: {
    fontSize: '16@ms0.2',
  },
  hi: {
    color: 'yellow',
    fontSize: '12@ms0.3',
  },
  name: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: '12@ms0.3',
  },
});
