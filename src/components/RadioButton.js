import React from 'react';
import { View } from 'react-native';
import themeStyle from 'src/styles/theme.style';

export default RadioButton = (props) => {
  return (
    <View style={[{
      height: 20,
      width: 20,
      borderRadius: 10,
      borderWidth: 1,
      borderColor: themeStyle.BLUE_COLOR_LIGHT,
      alignItems: 'center',
      justifyContent: 'center',
    }, props.style]}>
      {
        props.selected ?
          <View style={{
            height: 10,
            width: 10,
            borderRadius: 5,
            backgroundColor: themeStyle.BLUE_COLOR_LIGHT,
          }} />
          : null
      }
    </View>
  );
}
