import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { Dimensions, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { NAVIGATION } from 'src/constants';

const width = Dimensions.get('window').width;
const ItemVideo = ({ item, playlistId }) => {
  const navigation = useNavigation();
  const id = item?.contentDetails?.videoId ?? undefined;
  const thumbnail = item?.snippet?.thumbnails?.maxres?.url ?? undefined;
  const title = item?.snippet?.title ?? 'Chưa có tiêu đề';

  const handlePress = () => {
    navigation.navigate(NAVIGATION.SCR_VIDEO_DETAIL, { id, playlistId })
  }

  return (
    <TouchableOpacity onPress={handlePress}>
      <View style={styles.videoItem}>
        <View style={styles.videoImageContainer}>
          {thumbnail && <Image source={{
            uri: thumbnail,
          }}
            style={styles.videoImage}
          />}
        </View>
        <View style={styles.videoBottom}>
          {/* <View style={styles.avatarContainer}>
            {true && <Image
              style={styles.avatar}
              source={{ uri: 'https://static.tuoitre.vn/tto/i/s626/2015/08/12/hoathinh-11-8-4read-only-1439338173.jpg' }}
            />}
          </View> */}
          <View style={styles.desc}>
            <Text style={styles.descText} numberOfLines={2}>{title}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  )
}
export default ItemVideo;
const styles = StyleSheet.create({
  videoItem: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: 8
  },
  videoImageContainer: {
    flex: 1,
    height: width / 2 / (16 / 9),
  },
  videoImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
  videoBottom: {
    height: 50,
    minHeight: 50,
    backgroundColor: 'white',
    paddingHorizontal: 10,
    paddingVertical: 8,
    display: 'flex',
    flexDirection: 'row',
  },
  avatarContainer: {
    alignItems: 'center',
    height: '100%',
    marginRight: 10
  },
  avatar: {
    borderColor: '#EEE',
    borderRadius: 19,
    width: 38,
    height: 38,
    resizeMode: 'cover',
    overflow: 'hidden'
  },
  desc: {
    flex: 1
  },
  descText: {
    fontSize: 12
  }
})