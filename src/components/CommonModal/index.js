import { connect } from 'react-redux';
import { hideModal, showModal } from 'src/appRedux/actions/modal';
import { CommonModalComponent } from './CommonModal';

const mapStateToProps = (state, ownProps) => ({ modal: state.modal, ...ownProps });

const mapDispatchToProps = dispatch => ({
  showModal: () => {
    dispatch(showModal());
  },
  hideModal: () => {
    dispatch(hideModal());
  }
});

export const CommonModal = connect(mapStateToProps, mapDispatchToProps)(CommonModalComponent);



