import { Provider as AntdProvider } from '@ant-design/react-native';
import { default as React } from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/es/integration/react';
import App from './App';
import configureStore from './src/appRedux/store';
const { store, persistor } = configureStore();

export default NextApp = () => {
  return (
    <Provider store={store}>
      <AntdProvider>
        <PersistGate
          loading={null}
          persistor={persistor}
        >
          <App />
        </PersistGate>
      </AntdProvider>
    </Provider>
  );
}