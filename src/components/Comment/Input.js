import { ActivityIndicator } from '@ant-design/react-native';
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import { useDispatch, useSelector } from 'react-redux';
import { commentSetItem } from 'src/appRedux/actions/comment';
import { trimTabHtml } from 'src/helper/common';
import TextareaItemCustom from '../TextAreaItemCustom';

export default function Input({ onSubmit }) {
  const [text, setText] = useState(undefined);
  const { addCommentReducer, updateCommentReducer, detailCommentReducer } = useSelector(state => state);
  const { loading: loadingAddComment } = addCommentReducer;
  const { loading: loadingEditComment } = updateCommentReducer;
  const { data: itemDetail } = detailCommentReducer;
  const id = itemDetail?.id ?? undefined;
  const loading = id ? loadingEditComment : loadingAddComment;
  const content = itemDetail?.content?.rendered || '';
  const dispatch = useDispatch();

  useEffect(() => {
    if (id) {
      const noiDung = trimTabHtml(content)
      setText(noiDung);
    } else {
      setText('');
    }
  }, [id])

  const onChangeText = (text) => setText(text);

  const resetItem = () => {
    dispatch(commentSetItem(null));
    setText('');
  }

  const submit = () => {
    if (text) {
      onSubmit({
        values: { content: text, id },
        clearText: () => {
          setText('');
          resetItem();
        }
      });
    } else {
      alert('Hãy nhập góp ý');
    }
  };

  return (

    <View style={styles.container}>
      {/* Comment input field */}
      <View style={styles.inputContainer}>
        <TextareaItemCustom
          rows={3}
          placeholder="Thêm góp ý..."
          keyboardType="twitter" // keyboard with no return button
          value={text}
          onChangeText={onChangeText} // handle input changes
        />
      </View>

      {/* Post button */}
      <TouchableOpacity
        style={styles.button}
      >

        <View style={styles.action}>
          <Text
            onPress={resetItem}
            style={[styles.textHuy, !text ? styles.inactive : []]}
          >
            Huỷ
              </Text>
          <Text
            style={[styles.text, !text ? styles.inactive : []]}
            onPress={submit}
          >
            Gửi {loading ? <ActivityIndicator size={13} /> : <IconFontAwesome name="send-o" style={styles.iconSend} />}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#EEE',
    alignItems: 'center',
    paddingBottom: 10
  },
  inputContainer: {
    marginRight: 10,
    flex: 1,
  },
  input: {
    fontSize: 13,
  },
  iconSend: {
    fontSize: 13,
    alignSelf: 'center'
  },
  button: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  inactive: {
    opacity: 0.6
  },
  text: {
    color: '#3F51B5',
    fontWeight: 'bold',
    fontFamily: 'Avenir',
    textAlign: 'center',
    fontSize: 15,
  },
  textHuy: {
    color: 'red',
    marginRight: 20
  },
  action: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  }
});