import { Button, List, Progress, WhiteSpace } from '@ant-design/react-native';
import React, { useEffect, useState } from 'react';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';

const Item = List.Item;
export default function UploadDoc({
  onChange,
  listFile,
  typeUpload = 'single',
  accept = 'doc,docx,pdf,ppt,pptx',
  uploadFile,
}) {
  const [percent, setPercent] = useState(0);
  const [arrayFile, setArrayFile] = useState(listFile);
  const [start, setStart] = useState(false);

  useEffect(() => {
    if (percent === 100) {
      setStart(false);
    }
  }, [percent]);

  /**
   * Xử lý khi nhấn upload
   *
   * @param {String} value
   */
  const handleOnChangeFile = (value) => {
    let data = value;
    if (typeUpload === 'single') {
      data = [value];
    }
    setArrayFile(data);
    onChange(data);
  };

  /**
   * Thay đổi file khi chỉnh sửa
   *
   * @param {Object} file
   * @param {*} index
   */
  const handleReplaceFile = (file, index) => {
    const newData = [...arrayFile];
    newData[index] = file;
    setArrayFile(newData);
    onChange(newData);
  };

  /**
   * Lấy phần trăm thanh tiến trình
   *
   * @param {Number} percent
   */
  const handleUpLoadProgress = (percent) => {
    setPercent(percent);
  };

  /**
   * Hiển thị icon theo đường dẫn
   *
   * @param {String} path
   * @return {*}
   */
  const renderIconFile = (path) => {
    const allowedDocx = /(\.doc|\.docx)$/i;
    const allowedPDF = /(\.pdf)$/i;
    const allowedPtt = /(\.ppt|\.pptx)$/i;
    switch (true) {
      case allowedDocx.test(path):
        return <IconAntDesign name="wordfile1" color="blue" size={20} />;
      case allowedPDF.test(path):
        return <IconAntDesign name="pdffile1" color="red" size={20} />;
      case allowedPtt.test(path):
        return <IconFontAwesome name="file-powerpoint-o" color="orange" size={20} />;
      default:
        return <IconAntDesign name="filetext1" size={20} color="#3d3d3d" />;
    }
  };

  /**
   * Handle khi nhấn xoá 1 file
   *
   * @param {String} id
   */
  const handleDeleteFile = (id) => {
    const newData = arrayFile.filter((item) => item.id !== id);
    setArrayFile(newData);
    onChange(newData);
  };

  /**
   * Hiển thị icon xoá
   *
   * @param {Object} item
   * @return {*}
   */
  const renderIconDelete = (item) => {
    return (
      <TouchableOpacity onPress={() => handleDeleteFile(item.id)}>
        <IconAntDesign name="close" color="black" size={18} />
      </TouchableOpacity>
    );
  };

  /**
   * Chuyển string 'pdf,doc,...' sang ['pdf','doc',...]
   * @param {String} arrString
   * @return {*}
   */
  const convertStringToListTypeFile = (arrString) => {
    return arrString.reduce((acc, currentValue) => {
      if (currentValue === 'pdf') {
        acc = [...acc, DocumentPicker.types.pdf];
      } else if (currentValue === 'doc') {
        acc = [...acc, DocumentPicker.types.doc];
      } else if (currentValue === 'docx') {
        acc = [...acc, DocumentPicker.types.docx];
      } else if (currentValue === 'ppt') {
        acc = [...acc, DocumentPicker.types.pptx];
      } else if (currentValue === 'pptx') {
        acc = [...acc, DocumentPicker.types.pptx];
      } else {
        return acc;
      }
      return acc;
    }, []);
  };

  /**
   * Handle Upload 1 file
   *
   * @param {String} index
   */
  const uploadSingle = async (index) => {
    try {
      const typeFile = convertStringToListTypeFile(accept.split(','));
      const res = await DocumentPicker.pick({
        type: typeFile,
      });
      setStart(true);
      setPercent(0);
      const fileResponse = await uploadFile(res, handleUpLoadProgress);
      if (index) {
        fileResponse && handleReplaceFile(fileResponse, index);
      } else {
        fileResponse && handleOnChangeFile(fileResponse);
      }
    } catch (err) {
      setStart(false);
      setPercent(0);
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  /**
   * Handle Upload mutil file
   *
   */
  const uploadMutilple = async () => {
    try {
      const typeFile = convertStringToListTypeFile(accept.split(','));
      const results = await DocumentPicker.pickMultiple({
        type: typeFile,
      });
      const data = await Promise.all(
        results.map((item) => {
          return uploadFile(item);
        })
      );
      data && handleOnChangeFile(data);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  return (
    <View>
      <Button
        style={{ width: 140, height: 36 }}
        onPress={
          typeUpload == 'single' ? uploadSingle : typeUpload === 'multiple' ? uploadMutilple : null
        }
      >
        <IconAntDesign name="cloudupload" size={14} style={{ marginRight: 10 }} />
        <Text style={{ fontSize: 12 }}>Tải file lên</Text>
      </Button>
      {start && typeUpload === 'single' && (
        <>
          <WhiteSpace />
          <View style={{ height: 4, flex: 1 }}>
            <Progress percent={percent} />
          </View>
        </>
      )}
      {Array.isArray(arrayFile) && arrayFile.length > 0 && (
        <>
          <WhiteSpace />
          <ScrollView
            automaticallyAdjustContentInsets={false}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
          >
            <List renderHeader={'Danh sách file đính kèm'}>
              {arrayFile.map((item, index) => {
                return (
                  <Item
                    key={index}
                    onPress={() => uploadSingle(index)}
                    thumb={renderIconFile(item.url)}
                    extra={renderIconDelete(item)}
                  >
                    <Text style={{ marginLeft: 10 }}>{item.title}</Text>
                  </Item>
                );
              })}
            </List>
          </ScrollView>
        </>
      )}
    </View>
  );
}
