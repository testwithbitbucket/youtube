import { LOGIN_FAILED, LOGIN_REQUEST, LOGIN_RESPONSE, LOG_OUT, SET_INIT } from '../actionTypes';

export function requestLogin(username, password, routeName) {
  return {
    type: LOGIN_REQUEST,
    username,
    password,
    routeName
  };
}

export function loginFailed() {
  return {
    type: LOGIN_FAILED,
  };
}

export function onLoginResponse(response) {
  return {
    type: LOGIN_RESPONSE,
    response,
  };
}

export function logOut() {
  return {
    type: LOG_OUT,
  };
}

export function setInit(isAuth) {
  return {
    type: SET_INIT,
    isAuth
  };
}
