import { Icon } from '@ant-design/react-native';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function CardDetail({ title, icon, style }) {
  if (title) {
    return (
      <View style={[styles.container, style]}>
        <Icon name={icon} style={styles.icon} />
        <Text style={styles.textColor}>{title}</Text>
      </View>
    )
  }
  return null;
}

const styles = StyleSheet.create({
  container: {
    marginRight: 15,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  textColor: {
    color: '#7a7a7a',
  },
  icon: {
    marginRight: 4,
    color: '#7a7a7a',
    fontSize: 16
  },
});
