import { List } from '@ant-design/react-native';
import React from 'react';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import useModal from 'src/hooks/useModal';
import themeStyle from 'src/styles/theme.style';
import ModalPicker from './ModalPicker';
import RadioButton from './RadioButton';

const Item = List.Item;

const ListSelect = ({ value, data, onChange, title, reset, labelBold = false, heightModal }) => {
  const { isShowing, toggle } = useModal();

  /**
   * Xử lý khi chọn item
   * @param {Object} valueItem
   */
  const handleChangeList = (valueItem) => {
    onChange(valueItem);
    toggle();
  }

  /**
   * Hiển thị label value item
   * @return {*} 
   */
  const getLabel = () => {
    const item = data.find(item => item.value === value);
    return item && item.label || 'Mặc định';
  }

  /** 
   * Hiển thị danh sách item
   * @type {*} 
   */
  const option = data.map((item, index) => {
    return (
      <Item
        onPress={() => handleChangeList(item.value)}
        key={index}
        extra={
          <RadioButton selected={item.value === value} />
        }
      >
        {item.label}
      </Item>
    )
  })

  return (
    <View>
      <TouchableOpacity onPress={toggle} style={{
        backgroundColor: themeStyle.WHITE_COLOR,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 14,
        paddingVertical: 8,
      }}
      >
        <Text onPress={toggle} style={{ fontSize: 16, color: '#000', fontWeight: labelBold ? 'bold' : 'normal' }}>
          {title}
        </Text>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
          <Text style={{ fontSize: 16, color: '#a1a1a1' }}>{getLabel()}</Text>
          <Icon name="right" size={20} color='#a1a1a1' style={{ paddingTop: 2 }} />
        </View>
      </TouchableOpacity>
      <ModalPicker
        toggle={toggle}
        visible={isShowing}
        title={title}
        reset={reset}
        height={heightModal}
      >
        <ScrollView>
          <List>
            {option}
          </List>
        </ScrollView>
      </ModalPicker>
    </View>
  )
}

export default ListSelect;