import React from "react";
import { StyleSheet, View, Text } from "react-native";

const styles = StyleSheet.create({
  wrap: {
    marginTop: 5,
    marginBottom: 5,
  },
  badge: {
    position: 'absolute',
    right: -10,
    top: -6,
    backgroundColor: 'red',
    borderRadius: 8,
    width: 'auto',
    minWidth: 18,
    height: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  badgeText: {
    fontSize: 8,
    color: '#fff'
  }
});

const withBadge = (value, options = {}) => WrappedComponent =>
  class extends React.Component {
    render() {
      const { top = 0, right = 3, left = 0, bottom = 0, hidden = !value, ...badgeProps } = options;
      return (
        <View style={styles.wrap}>
          <WrappedComponent {...this.props} />
          {!hidden && (
            <View
              style={styles.badge}
              {...badgeProps}
            >
              <Text style={styles.badgeText}>{value}</Text>
            </View>
          )}
        </View>
      );
    }
  };

export default withBadge;