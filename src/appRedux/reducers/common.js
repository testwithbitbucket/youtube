import { DON_VI, DU_AN, TRANG_THAI_CONG_VIEC, LIST_USER } from 'src/constants/DataType';
import {
  TRANG_THAI_RESPONSE, TRANG_THAI_FAILED,
  DU_AN_RESPONSE, DU_AN_FAILED,
  DON_VI_RESPONSE, DON_VI_FAILED,
  LIST_USER_RESPONSE, LIST_USER_FAILED
} from "../actionTypes";

const initCommonData = {
  loading: false,
  data: []
}

const initData = {
  [TRANG_THAI_CONG_VIEC]: initCommonData,
  [DU_AN]: initCommonData,
  [DON_VI]: initCommonData,
  [LIST_USER]: initCommonData
};

const commonReducer = (state = initData, action) => {
  switch (action.type) {
    case TRANG_THAI_RESPONSE:
      return {
        ...state,
        [TRANG_THAI_CONG_VIEC]: {
          loading: false,
          data: action.response
        }
      };
    case TRANG_THAI_FAILED:
      return {
        ...state,
        [TRANG_THAI_CONG_VIEC]: { ...initCommonData }
      };
    case DU_AN_RESPONSE:
      return {
        ...state,
        [DU_AN]: {
          loading: false,
          data: action.response.data
        }
      };
    case DU_AN_FAILED:
      return {
        ...state,
        [DU_AN]: { ...initCommonData }
      };
    case DON_VI_RESPONSE:
      return {
        ...state,
        [DON_VI]: {
          loading: false,
          data: action.response.data
        }
      };
    case DON_VI_FAILED:
      return {
        ...state,
        [DON_VI]: { ...initCommonData }
      };
    case LIST_USER_RESPONSE:
      return {
        ...state,
        [LIST_USER]: {
          loading: false,
          data: action.response.data
        }
      };
    case LIST_USER_FAILED:
      return {
        ...state,
        [LIST_USER]: { ...initCommonData }
      };
    default:
      return state;
  }
};
export default commonReducer;