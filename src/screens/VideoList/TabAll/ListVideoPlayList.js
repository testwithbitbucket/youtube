import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import playlistItemApi from 'src/api/playlistItem';
import ItemVideo from './ItemVideo';

const ListVideoPlayList = ({ id }) => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);

  useEffect(() => {
    let isCancelled = false;
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();
    const getListItemVideo = async (id) => {
      try {
        setLoading(true);
        const res = await playlistItemApi.getPlayListItem({ playlistId: id, maxResults: 2, cancelToken: source.token });
        if (res.status === 200 && !isCancelled) {
          setData(res.data.items);
        } else {
          throw new Error('có lỗi xảy ra')
        }
      } catch (error) {
        if (axios.isCancel(error)) {
          console.log("cancelled");
        } else {
          throw error;
        }
      } finally {
        if (!isCancelled) {
          setLoading(false);
        }
      }
    };

    if (id) {
      getListItemVideo(id);
    }
    return () => {
      source.cancel();
      isCancelled = true
    };
  }, [id])

  return (
    <View style={styles.container}>
      {data.map((item, index) => {
        return (
          <View style={styles.videoItem} key={index} >
            <ItemVideo item={item} playlistId={id} />
          </View>
        )
      })}
    </View>
  )
}
export default ListVideoPlayList;
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    marginLeft: -4,
    marginRight: -4
  },
  videoItem: {
    width: '50%',
    paddingRight: 4,
    paddingLeft: 4
  }
})