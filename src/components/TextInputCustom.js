import React from 'react'
import { StyleSheet, TextInput } from 'react-native'
import themeStyle from 'src/styles/theme.style'

export default function TextInputCustom(props) {
  return <TextInput
    {...props}
    style={styles.inputItem}
    placeholderTextColor={themeStyle.COLOR_PLACEHOLDER}
  />
}
const styles = StyleSheet.create({
  inputItem: {
    borderColor: themeStyle.GREY_COLOR_LIGHT,
    backgroundColor: themeStyle.GREEN_COLOR_LIGHT,
    borderWidth: 1,
    fontSize: 14,
    paddingHorizontal: 8,
    height: 38
  }
})