import { apiCaller, getChannelYoutube, getKeyApiYoutube } from 'src/helper/AxiosClient';

const getPlayList = async (data) => {
  const KEY_CHANNEL_YOUTUBE = await getChannelYoutube();
  const KEY_API_YOUTUBE = await getKeyApiYoutube();
  const config = {
    method: 'GET',
    url: `playlists`,
    params: {
      ...data,
      channelId: KEY_CHANNEL_YOUTUBE,
      key: KEY_API_YOUTUBE,
    },
  };
  const res = await apiCaller(config);
  return res;
};

export default {
  getPlayList,
};
