import { Dimensions, Platform } from 'react-native';

const IPAD_WIDTH = 768;
const IPAD_HEIGHT = 1024;

const { height: W_HEIGHT, width: W_WIDTH } = Dimensions.get('window');

let isIPad = false;
if (Platform.isPad) {
  isIPad = W_WIDTH >= IPAD_WIDTH && W_HEIGHT >= IPAD_HEIGHT;
}

export { isIPad };