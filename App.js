import { default as React } from 'react';
import { RootNavigator } from './src/navigation';
import { SafeAreaView, StatusBar, View, StyleSheet } from 'react-native';
import themeStyle from './src/styles/theme.style';

const MyStatusBar = ({ backgroundColor, ...props }) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <SafeAreaView>
      <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </SafeAreaView>
  </View>
);

export default function App() {
  return (
    <>
      <MyStatusBar backgroundColor={themeStyle.BLUE_COLOR_BOLD} barStyle="light-content" />
      <SafeAreaView style={styles.content}>
        <RootNavigator />
      </SafeAreaView>
    </>
  )
}

const STATUSBAR_HEIGHT = StatusBar.currentHeight;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  content: {
    flex: 1,
  },
});