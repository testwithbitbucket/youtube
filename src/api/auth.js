import axios from 'axios';
import { apiCaller } from 'src/helper/AxiosClient';
const login = function* login(username, password) {
  const config = {
    method: 'POST',
    url: 'https://qlcv.webquangnam.com/wp-json/jwt-auth/v1/token',
    headers: {
      'Content-Type': 'application/json',
      accept: 'application/json',
    },
    data: { username, password },
  };
  return yield axios(config)
    .then((response) => ({ response }))
    .catch((error) => ({ error }));
};
const updateTokenThietBi = (token) => {
  const config = {
    method: 'POST',
    url: 'users/me',
    headers: {
      'Content-Type': 'application/json',
      accept: 'application/json',
    },
    data: { tokenThietBi: token },
  };
  return apiCaller(config)
    .then((response) => ({ response }))
    .catch((error) => ({ error }));
};
export default {
  login,
  updateTokenThietBi,
};
