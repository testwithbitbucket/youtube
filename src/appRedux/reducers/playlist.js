import {
  PLAY_LIST_GET_ERROR, PLAY_LIST_GET_SET_LOADING, PLAY_LIST_GET_SUCCESS, PLAY_LIST_SET_IS_LAST
} from '../actionTypes';


const initDataGet = {
  items: [],
  loading: false,
  nextPageToken: undefined,
  prevPageToken: undefined,
  pageInfo: {
    totalResults: 0,
    resultsPerPage: 0
  },
  meta: {
    maxResults: 2,
    pageToken: undefined,
    part: 'snippet,contentDetails,player'
  }

};

export const getPlaylistReducer = (state = initDataGet, action) => {
  switch (action.type) {
    case PLAY_LIST_SET_IS_LAST: {
      return {
        ...state,
        isLast: action.isLast
      }
    }
    case PLAY_LIST_GET_SUCCESS:
      return {
        ...state,
        items: action.payload.items,
        pageInfo: action.payload.pageInfo
      };
    case PLAY_LIST_GET_ERROR:
      return {
        ...state,
        ...initDataGet,
        err: action.err
      };
    case PLAY_LIST_GET_SET_LOADING:
      return {
        ...state,
        loading: action.payload
      };
    default:
      return state;
  }
};
