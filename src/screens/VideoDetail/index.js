import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { ActivityIndicator, Dimensions, Image, StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import Youtube from 'react-native-youtube';
import videoApi from 'src/api/video';
import { KEY_API_YOUTUBE } from 'src/helper/AxiosClient';
import ListVideo from 'src/screens/VideoList/TabSingle/ListVideo';
import themeStyle from 'src/styles/theme.style';

const width = Dimensions.get('window').width;
export const VideoDetail = ({ route }) => {
  const [loading, setLoading] = useState(false);
  const id = route?.params?.id ?? undefined;
  const playlistId = route?.params?.playlistId ?? undefined;
  const [item, setItem] = useState(null);
  const title = item?.snippet?.title ?? 'Chưa có tiêu đề';

  useEffect(() => {
    let isCancelled = false;
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();
    const getItemVideo = async (id) => {
      try {
        setLoading(true);
        const res = await videoApi.getItemVideoByVideo({ id, cancelToken: source.token });
        if (res.status === 200 && !isCancelled) {
          const item = res?.data?.items?.[0] ?? null;
          if (item) {
            setItem(item);
          }
        } else {
          throw new Error('có lỗi xảy ra');
        }
      } catch (error) {
        if (axios.isCancel(error)) {
          console.log('cancelled');
        } else {
          throw error;
        }
      } finally {
        if (!isCancelled) {
          setLoading(false);
        }
      }
    };
    if (id) {
      getItemVideo(id);
    }
    return () => {
      source.cancel();
      isCancelled = true;
    };
  }, [id]);

  return (
    <View style={styles.container}>
      {loading ? (
        <ActivityIndicator size="large" color={themeStyle.BLUE_COLOR_BOLD} />
      ) : (
        <View style={styles.videoItem}>
          <View style={styles.videoPlayer}>
            {id && (
              <Youtube
                apiKey={KEY_API_YOUTUBE}
                videoId={id}
                loop
                controls={2}
                play={true}
                style={{ height: width / (16 / 9) }}
              />
            )}
          </View>
          <View style={styles.videoBottom}>
            <View style={styles.avatarContainer}>
              {true && (
                <Image
                  style={styles.avatar}
                  source={{
                    uri: 'https://static.tuoitre.vn/tto/i/s626/2015/08/12/hoathinh-11-8-4read-only-1439338173.jpg',
                  }}
                />
              )}
            </View>
            <View style={styles.desc}>
              <Text numberOfLines={3}>{title}</Text>
            </View>
          </View>
          <View style={styles.videoOther}>
            <View style={styles.videoOtherTitle}>
              <Icon style={styles.videoOtherIcon} name="folder-video" />
              <Text style={styles.videoOtherText}>Video trong danh sách</Text>
            </View>
            <ListVideo id={playlistId} />
          </View>
        </View>
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  videoItem: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: 8,
    flex: 1,
  },
  videoPlayer: {},
  player: {
    height: '100%',
    width: '100%',
  },
  videoBottom: {
    minHeight: 76,
    backgroundColor: 'white',
    paddingHorizontal: 10,
    paddingVertical: 8,
    display: 'flex',
    flexDirection: 'row',
  },
  avatarContainer: {
    alignItems: 'center',
    height: '100%',
    marginRight: 10,
  },
  avatar: {
    borderColor: '#EEE',
    borderRadius: 19,
    width: 38,
    height: 38,
    resizeMode: 'cover',
    overflow: 'hidden',
  },
  desc: {
    flex: 1,
  },
  videoOther: {
    flex: 1,
  },
  videoOtherTitle: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    backgroundColor: 'white',
    marginVertical: 4,
  },
  videoOtherText: {
    fontWeight: 'bold',
  },
  videoOtherIcon: {
    fontSize: 16,
    marginRight: 10,
  },
});
