import { Button } from '@ant-design/react-native';
import moment from 'moment';
import React, { useState } from 'react';
import { Pressable, StyleSheet, Text, View } from 'react-native';
import DatePicker from 'react-native-date-picker';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import useModal from 'src/hooks/useModal';
import themeStyle from 'src/styles/theme.style';
import ModalPicker from './ModalPicker';

export default function DatePickerCustom(props) {
  const { isShowing, toggle } = useModal();
  const [date, setDate] = useState(props.value);
  const [title, setTitle] = useState(props.value);
  const dateNow = moment(new Date()).format("DD/MM/YYYY");

  /**
   * Thay đổi ngày
   * @param {String} value
   */
  const onDateChange = (value) => {
    const data = moment(value).format("DD/MM/YYYY");
    setDate(data);
  }

  /**
   * Reset controll thành rỗng
   */
  const resetDate = () => {
    setDate(null)
    setTitle("");
  }

  /**
   * Reset controll thành ngày hiện tại
   */
  const resetNow = () => {
    setDate(dateNow);
    setTitle(dateNow);
  }

  /**
   * Hàm handle khi nhấn chọn
   * @param {*} values
   */
  const onSubmitDate = () => {
    toggle();
    if (!date) {
      setDate(dateNow);
      props.onChange(dateNow);
      setTitle(dateNow);
    } else {
      setDate(date);
      props.onChange(date);
      setTitle(date);
    }
  }

  return (
    <View style={{ flex: 1 }}>
      <Text style={styles.inputItemDate} onPress={toggle}>{title ? title : <Text style={styles.placeHolderText}>Chọn ngày</Text>}</Text>
      <Pressable style={styles.iconAfter} onPress={title ? resetDate : toggle}>
        {title ? <IconAntDesign name="close" size={14} /> : <IconAntDesign name="calendar" size={14} />}
      </Pressable>
      <ModalPicker
        toggle={toggle}
        visible={isShowing}
        title="Chọn ngày"
        reset={resetNow}
        height={320}
      >
        <View style={styles.containerDate}>
          <DatePicker
            mode="date"
            locale="vi"
            date={date ? moment(date, 'DD/MM/YYYY').toDate() : new Date()}
            onDateChange={onDateChange}
            androidVariant="nativeAndroid"
          >
          </DatePicker>
        </View>
        <View style={styles.groupBtn}>
          <Button style={styles.btnForm} onPress={onSubmitDate} type="primary">
            Chọn
          </Button>
        </View>
      </ModalPicker>
    </View>
  )
}
const styles = StyleSheet.create({
  containerDate: {
    flex: 1,
    justifyContent: "center",
    alignItems: 'center',
    backgroundColor: themeStyle.WHITE_COLOR,
    paddingHorizontal: 14,
    marginBottom: 10
  },
  inputItemDate: {
    borderColor: themeStyle.GREY_COLOR_LIGHT,
    backgroundColor: themeStyle.GREEN_COLOR_LIGHT,
    borderWidth: 1,
    fontSize: 14,
    paddingHorizontal: 8,
    height: 38,
    lineHeight: 38
  },
  placeHolder: {
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 6
  },
  placeHolderText: {
    color: '#a1a1a1'
  },
  iconAfter: {
    position: 'absolute',
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
    height: '100%'
  },
  groupBtn: {
    padding: 10
  },
  btnForm: {
    width: '100%'
  }
})