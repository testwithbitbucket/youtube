import { TextareaItem } from '@ant-design/react-native';
import React from 'react';
import { StyleSheet } from 'react-native';
import themeStyle from 'src/styles/theme.style';

export default function TextareaItemCustom(props) {
  return <TextareaItem
    {...props}
    style={styles.inputArea}
    placeholderTextColor={themeStyle.COLOR_PLACEHOLDER}
  />
}
const styles = StyleSheet.create({
  inputArea: {
    backgroundColor: themeStyle.GREEN_COLOR_LIGHT,
    borderWidth: 1,
    borderColor: themeStyle.GREY_COLOR_LIGHT,
    fontSize: 14,
    paddingHorizontal: 6
  },
})