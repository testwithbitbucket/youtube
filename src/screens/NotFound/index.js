import React from 'react';
import { Text, View } from 'react-native';

export const NotFound = () => {
	return (
		<View flex={1} justifyContent="center">
			<Text variant="body" textAlign="center">
				NotFound
        </Text>
		</View>
	);
};